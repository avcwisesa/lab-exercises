from collections import namedtuple
import multiprocessing as mp
import random
import time

VALUES = (100, 200, 500, 1000)
Coin = namedtuple('Coin', ['value'])


def reader(queue, start_time):
    termination_threshold = 25
    termination_count = 0
    reader_sum = 0

    while termination_count < termination_threshold:
        if queue.empty():
            print("[Process {}] Waiting for new items...".format(
                  mp.current_process().name))
            time.sleep(random.random() * 0.50)
            termination_count += 1
        else:
            termination_count = 0
            coin = queue.get()
            reader_sum += coin.value
            time.sleep(random.random() * 0.50)
            print("[{:5f}] : [Process {}] Read coin ({})".format(
                  (time.time() - start_time), mp.current_process().name, str(coin.value)))

    print("[Process {}] Total value read: {}".format(
          mp.current_process().name, reader_sum))
    print("[Process {}] Exiting...".format(mp.current_process().name))


def writer(count, queue, start_time):
    writer_sum = 0

    for ii in range(count):
        coin = Coin(random.choice(VALUES))
        queue.put(coin)
        writer_sum += coin.value

        # No need to prepend string with process name since this
        # function is executed in main interpreter thread
        print("[{:5f}] Put coin ({}) into queue".format((time.time() - start_time), coin.value))
        time.sleep(random.random() * 0.050)

    print('Total value written: ' + str(writer_sum))


if __name__ == '__main__':
    start_time = time.time()
    count = 100
    queue = mp.Queue()  # Queue class from multiprocessing module

    n = 2
    readers = []
    for i in range(1,n+1):

        readers.append(mp.Process(target=reader, name='Reader {}'.format(i), args=(queue, start_time,)))
        readers[i-1].daemon = True
        readers[i-1].start()
    

    writer(count, queue, start_time)

    for i in range(1,n+1):
        readers[i-1].join()
    
    end_time = time.time()

    print('Total running time: ' + str(end_time - start_time))
