from collections import namedtuple
import multiprocessing as mp
import random
import time

from concurrent.futures import ProcessPoolExecutor, as_completed

VALUES = (100, 200, 500, 1000)
Coin = namedtuple('Coin', ['value'])


def reader(queue):
    termination_threshold = 25
    termination_count = 0
    reader_sum = 0
    
    while termination_count < termination_threshold:       

        if len(queue) == 0:
            print("[Process {}] Waiting for new items...".format(
                  mp.current_process().name))
            time.sleep(random.random() * 0.50)
            termination_count += 1
        else:
            termination_count = 0
            # print(queue)
            coin = queue.pop()
            # print('abis print')
            reader_sum += coin.value
            time.sleep(random.random() * 0.50)
            print("[Process {}] Read coin ({})".format(
                  mp.current_process().name, str(coin.value)))

    print("[Process {}] Total value read: {}".format(
          mp.current_process().name, reader_sum))
    print("[Process {}] Exiting...".format(mp.current_process().name))


def writer(count, futures):
    writer_sum = 0
    queue = []

    for ii in range(count):
        coin = Coin(random.choice(VALUES))
        queue.append(coin)
        writer_sum += coin.value

        # No need to prepend string with process name since this
        # function is executed in main interpreter thread
        print("Put coin ({}) into queue".format(coin.value))
        time.sleep(random.random() * 0.050)

    with ProcessPoolExecutor() as executor:
        futures.append(executor.submit(reader, queue))

    print('Total chunk_value written: ' + str(writer_sum))
    
    return {'value':writer_sum, 'queue':queue}


if __name__ == '__main__':
    start_time = time.time()
    count = 100
    # queue = mp.Queue()  # Queue class from multiprocessing module
    
    futures = []
    future_inserts = []
    n = 5
    total = 0
    with ProcessPoolExecutor() as executor:
        for i in range(1,n+1):
            future_inserts.append(executor.submit(writer, int(max(count-(count/n),0)), futures))
            count -= (count/n)
            if count <= 0:
                break
            
    
    for process in as_completed(future_inserts):
        total += process.result()['value']

    print('Total written: {}'.format(total))
    
    for process in as_completed(futures):
        pass

    end_time = time.time()

    print('Total running time: ' + str(end_time - start_time))
